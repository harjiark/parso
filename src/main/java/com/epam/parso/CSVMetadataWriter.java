package com.epam.parso;

import java.io.IOException;
import java.util.List;

public abstract interface CSVMetadataWriter
{
  public abstract void writeMetadata(List<Column> paramList)
    throws IOException;

  public abstract void writeSasFileProperties(SasFileProperties paramSasFileProperties)
    throws IOException;
}