package com.epam.parso;

import java.io.IOException;
import java.util.List;

public abstract interface CSVDataWriter
{
  public abstract void writeRow(List<Column> paramList, Object[] paramArrayOfObject)
    throws IOException;

  public abstract void writeRowsArray(List<Column> paramList, Object[][] paramArrayOfObject)
    throws IOException;

  public abstract void writeColumnNames(List<Column> paramList)
    throws IOException;
}