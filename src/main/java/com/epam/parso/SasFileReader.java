package com.epam.parso;

import java.io.IOException;
import java.util.List;

public abstract interface SasFileReader
{
  public abstract List<Column> getColumns();

  public abstract Object[][] readAll();

  public abstract Object[] readNext()
    throws IOException;

  public abstract SasFileProperties getSasFileProperties();
}