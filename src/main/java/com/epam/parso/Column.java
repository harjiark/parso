package com.epam.parso;

public class Column
{
  private final int id;
  private final String name;
  private final String label;
  private final String format;
  private final Class<?> type;
  private final int length;

  public Column(int id, String name, String label, String format, Class<?> type, int length)
  {
    this.id = id;
    this.name = name;
    this.label = label;
    this.format = format;
    this.type = type;
    this.length = length;
  }

  public int getId()
  {
    return this.id;
  }

  public String getName()
  {
    return this.name;
  }

  public String getFormat()
  {
    return this.format;
  }

  public Class<?> getType()
  {
    return this.type;
  }

  public String getLabel()
  {
    return this.label;
  }

  public int getLength()
  {
    return this.length;
  }
}