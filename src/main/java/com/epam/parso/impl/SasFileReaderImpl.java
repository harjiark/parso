package com.epam.parso.impl;

import com.epam.parso.Column;
import com.epam.parso.SasFileProperties;
import com.epam.parso.SasFileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SasFileReaderImpl
  implements SasFileReader
{
  private static final Logger LOGGER = LoggerFactory.getLogger(SasFileReaderImpl.class);
  private final SasFileParser sasFileParser;

  public SasFileReaderImpl(InputStream inputStream)
  {
    this.sasFileParser = new SasFileParser.Builder().sasFileStream(inputStream).build();
  }

  public SasFileReaderImpl(InputStream inputStream, String encoding)
  {
    this.sasFileParser = new SasFileParser.Builder().sasFileStream(inputStream).encoding(encoding).build();
  }

  public SasFileReaderImpl(InputStream inputStream, Boolean byteOutput)
  {
    this.sasFileParser = new SasFileParser.Builder().sasFileStream(inputStream).byteOutput(byteOutput).build();
  }

  public List<Column> getColumns()
  {
    return this.sasFileParser.getColumns();
  }

  public Object[][] readAll()
  {
    int rowNum = (int)getSasFileProperties().getRowCount();
    Object[][] result = new Object[rowNum][];
    for (int i = 0; i < rowNum; i++) {
      try {
        result[i] = readNext();
      } catch (IOException e) {
        if (LOGGER.isWarnEnabled()) {
          LOGGER.warn("I/O exception, skipping the rest of the file. Rows read: " + i + ". Expected number of rows from metadata: " + rowNum, e);
        }

        break;
      }
    }
    return result;
  }

  public Object[] readNext()
    throws IOException
  {
    return this.sasFileParser.readNext();
  }

  public SasFileProperties getSasFileProperties()
  {
    return this.sasFileParser.getSasFileProperties();
  }
}