package com.epam.parso.impl;

abstract interface Decompressor
{
  public abstract byte[] decompressRow(int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte);
}