package com.epam.parso.impl;

import java.io.IOException;
import java.io.Writer;

abstract class AbstractCSVWriter
{
  private static final String DEFAULT_DELIMITER = ",";
  private static final String DEFAULT_ENDLINE = "\n";
  private final Writer writer;
  private String delimiter = ",";

  private String endline = "\n";

  AbstractCSVWriter(Writer writer)
  {
    this.writer = writer;
  }

  AbstractCSVWriter(Writer writer, String delimiter)
  {
    this.writer = writer;
    this.delimiter = delimiter;
  }

  AbstractCSVWriter(Writer writer, String delimiter, String endline)
  {
    this.writer = writer;
    this.delimiter = delimiter;
    this.endline = endline;
  }

  static void checkSurroundByQuotesAndWrite(Writer writer, String delimiter, String trimmedText)
    throws IOException
  {
    boolean containsDelimiter = stringContainsItemFromList(trimmedText, new String[] { delimiter, "\n", "\t", "\r", "\"" });
    String trimmedTextWithoutQuotesDuplicates = trimmedText.replace("\"", "\"\"");
    if ((containsDelimiter) && (trimmedTextWithoutQuotesDuplicates.length() != 0)) {
      writer.write("\"");
    }
    writer.write(trimmedTextWithoutQuotesDuplicates);
    if ((containsDelimiter) && (trimmedTextWithoutQuotesDuplicates.length() != 0))
      writer.write("\"");
  }

  private static boolean stringContainsItemFromList(String inputString, String[] items)
  {
    for (String item : items) {
      if (inputString.contains(item)) {
        return true;
      }
    }
    return false;
  }

  public Writer getWriter()
  {
    return this.writer;
  }

  public String getDelimiter()
  {
    return this.delimiter;
  }

  public String getEndline()
  {
    return this.endline;
  }
}