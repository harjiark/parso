package com.epam.parso.impl;

public class BytesAsHexFragment implements DumpFragment { 
	 
    private final int groupSize; 
    private final boolean insertSpan; 
 
    private static final char[] symbols = {'0', '1', '2', '3', '4', '5', '6', 
            '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}; 
 
    public BytesAsHexFragment(int groupSize) { 
        this(groupSize, false); 
    } 
 
    /**
     * Constructs a new instance, accepting the number of bytes that together make a group. 
     * 
     * @param groupSize 
     */ 
    public BytesAsHexFragment(int groupSize, boolean insertSpan) { 
        this.groupSize = groupSize; 
        this.insertSpan = insertSpan; 
    } 
 
    public int getSize(int numberOfBytes) { 
        return numberOfBytes * 2 + (numberOfBytes - 1) + ((numberOfBytes - 1) % groupSize); 
    } 
 
    public void dump(long lineNumber, byte[] buffer, int length, HexDumpTarget out) throws HexDumperException { 
        for (int i = 0; i < length; i++) { 
            append(i, symbols[hiword(buffer[i])], symbols[loword(buffer[i])], out, i + lineNumber * length); 
        } 
        for (int i = length; i < buffer.length; i++) { 
            append(i, ' ', ' ', out, i + lineNumber * length); 
        } 
    } 
 
    private void append(int i, char first, char second, HexDumpTarget out, long bytePos) { 
        if (i != 0) { 
            out.writeText(' '); 
            if (i % groupSize == 0) { 
                out.writeText(' '); 
            } 
        } 
        if (insertSpan) { 
            out.writeStartElement("span"); 
        } 
        out.writeText(first); 
        out.writeText(second); 
        if (insertSpan) { 
            out.writeEndElement();             
        } 
    } 
 
    /**
     * Returns the value of the first 4 bits of a byte. 
     * 
     * @param b The byte for which we need the first 4 bits. 
     * @return The value of the first 4 bits of a byte. 
     */ 
    private static int hiword(byte b) { 
        return (0xf0 & b) >> 4; 
    } 
 
    /**
     * Returns the value of the last 4 bits of a byte. 
     * 
     * @param b The byte for which we need the last 4 bits. 
     * @return The value of the last 4 bits of a byte. 
     */ 
    private static int loword(byte b) { 
        return (0x0f & b); 
    } 
}
