package com.epam.parso.impl;

import com.epam.parso.CSVDataWriter;
import com.epam.parso.Column;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class CSVDataWriterImpl extends AbstractCSVWriter
  implements CSVDataWriter
{
  private static final int ROUNDING_LENGTH = 13;
  private static final int ACCURACY = 15;
  private static final String DOUBLE_INFINITY_STRING = "Infinity";
  private static final String HOURS_OUTPUT_FORMAT = "%02d";
  private static final String MINUTES_OUTPUT_FORMAT = "%02d";
  private static final String SECONDS_OUTPUT_FORMAT = "%02d";
  private static final String TIME_DELIMETER = ":";
  private static final String DATE_FORMAT_YYMMDD = "YYMMDD";
  private static final String DATE_FORMAT_MMDDYY = "MMDDYY";
  private static final String DATE_FORMAT_DDMMYY = "DDMMYY";
  private static final String DATE_FORMAT = "DATE";
  private static final String DATE_TIME_FORMAT = "DATETIME";
  private static final List<String> TIME_FORMAT_STRINGS = Arrays.asList(new String[] { "TIME", "HHMM" });
  private static final int SECONDS_IN_MINUTE = 60;
  private static final int MINUTES_IN_HOUR = 60;
  private static final String ENCODING = "CP1252";
  private static final Map<String, String> DATE_OUTPUT_FORMAT_STRINGS;

  public CSVDataWriterImpl(Writer writer)
  {
    super(writer);
  }

  public CSVDataWriterImpl(Writer writer, String delimiter)
  {
    super(writer, delimiter);
  }

  public CSVDataWriterImpl(Writer writer, String delimiter, String endline)
  {
    super(writer, delimiter, endline);
  }

  private static String convertDateElementToString(Date currentDate, String format)
  {
    String valueToPrint = "";
    SimpleDateFormat dateFormat = new SimpleDateFormat((String)DATE_OUTPUT_FORMAT_STRINGS.get(format));
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    if (currentDate.getTime() != 0L) {
      valueToPrint = dateFormat.format(Long.valueOf(currentDate.getTime()));
    }
    return valueToPrint;
  }

  private static String convertTimeElementToString(Long secondsFromMidnight)
  {
    return String.format("%02d", new Object[] { Long.valueOf(secondsFromMidnight.longValue() / 60L / 60L) }) + ":" + 
      String.format("%02d", new Object[] { 
      Long.valueOf(secondsFromMidnight
      .longValue() / 60L % 60L) }) + ":" + 
      String.format("%02d", new Object[] { 
      Long.valueOf(secondsFromMidnight
      .longValue() % 60L) });
  }

  private static String convertDoubleElementToString(Double value)
  {
    String valueToPrint = String.valueOf(value);
    if (valueToPrint.length() > 13) {
      int lengthBeforeDot = (int)Math.ceil(Math.log10(Math.abs(value.doubleValue())));
      BigDecimal bigDecimal = new BigDecimal(value.doubleValue());
      bigDecimal = bigDecimal.setScale(15 - lengthBeforeDot, 4);
      valueToPrint = String.valueOf(bigDecimal.doubleValue());
    }
    valueToPrint = trimZerosFromEnd(valueToPrint);
    return valueToPrint;
  }

  private static String trimZerosFromEnd(String string)
  {
    return string.contains(".") ? string.replaceAll("0*$", "").replaceAll("\\.$", "") : string;
  }

  public void writeRow(List<Column> columns, Object[] row)
    throws IOException
  {
    if (row == null) {
      return;
    }

    Writer writer = getWriter();
    for (int currentColumnIndex = 0; currentColumnIndex < columns.size(); currentColumnIndex++) {
      if (row[currentColumnIndex] != null) {
        if (row[currentColumnIndex].getClass().getName().compareTo(new byte[0]
          .getClass().getName()) == 0) {
          checkSurroundByQuotesAndWrite(writer, getDelimiter(), new String((byte[])(byte[])row[currentColumnIndex], "CP1252"));
        }
        else {
          processEntry(columns, row, currentColumnIndex);
        }
      }
      if (currentColumnIndex != columns.size() - 1) {
        writer.write(getDelimiter());
      }
    }

    writer.write(getEndline());
    writer.flush();
  }

  public void writeRowsArray(List<Column> columns, Object[][] rows)
    throws IOException
  {
    for (Object[] currentRow : rows) {
      if (currentRow == null) break;
      writeRow(columns, currentRow);
    }
  }

  public void writeColumnNames(List<Column> columns)
    throws IOException
  {
    Writer writer = getWriter();
    for (int i = 0; i < columns.size(); i++) {
      checkSurroundByQuotesAndWrite(writer, getDelimiter(), ((Column)columns.get(i)).getName());
      if (i != columns.size() - 1) {
        writer.write(getDelimiter());
      }
    }
    writer.write(getEndline());
  }

  private void processEntry(List<Column> columns, Object[] row, int currentColumnIndex)
    throws IOException
  {
    if (!String.valueOf(row[currentColumnIndex]).contains("Infinity"))
    {
      String valueToPrint;
      if (row[currentColumnIndex].getClass() == Date.class) {
        valueToPrint = convertDateElementToString((Date)row[currentColumnIndex], 
          ((Column)columns
          .get(currentColumnIndex))
          .getFormat());
      }
      else
      {
        if (TIME_FORMAT_STRINGS.contains(((Column)columns.get(currentColumnIndex)).getFormat())) {
          valueToPrint = convertTimeElementToString((Long)row[currentColumnIndex]);
        } else {
          valueToPrint = String.valueOf(row[currentColumnIndex]);
          if (row[currentColumnIndex].getClass() == Double.class) {
            valueToPrint = convertDoubleElementToString((Double)row[currentColumnIndex]);
          }
        }
      }

      checkSurroundByQuotesAndWrite(getWriter(), getDelimiter(), valueToPrint);
    }
  }

  static
  {
    Map tmpMap = new HashMap();
    tmpMap.put("YYMMDD", "yyyy-MM-dd");
    tmpMap.put("MMDDYY", "MM/dd/yyyy");
    tmpMap.put("DDMMYY", "dd/MM/yyyy");
    tmpMap.put("DATE", "ddMMMyyyy");
    tmpMap.put("DATETIME", "yyyy-MM-dd HH:mm:ss");
    DATE_OUTPUT_FORMAT_STRINGS = Collections.synchronizedMap(tmpMap);
  }
}