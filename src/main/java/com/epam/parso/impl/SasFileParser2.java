//package com.epam.parso.impl;
//
//import com.epam.parso.Column;
//import com.epam.parso.SasFileProperties;
//import java.io.DataInputStream;
//import java.io.EOFException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public final class SasFileParser2
//{
//  private static final Logger LOGGER = LoggerFactory.getLogger(SasFileParser.class);
//  private static final Map<Long, SubheaderIndexes> SUBHEADER_SIGNATURE_TO_INDEX;
//  private static final Map<String, Decompressor> LITERALS_TO_DECOMPRESSOR = new HashMap();
//  private final DataInputStream sasFileStream;
//  private final Boolean byteOutput;
//
//  static
//  {
//    Map<Long, SubheaderIndexes> tmpMap = new HashMap();
//    tmpMap.put(Long.valueOf(-134744073L), SubheaderIndexes.ROW_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-151587082L), SubheaderIndexes.COLUMN_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-1024L), SubheaderIndexes.SUBHEADER_COUNTS_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-3L), SubheaderIndexes.COLUMN_TEXT_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-1L), SubheaderIndexes.COLUMN_NAME_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-4L), SubheaderIndexes.COLUMN_ATTRIBUTES_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-1026L), SubheaderIndexes.FORMAT_AND_LABEL_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-2L), SubheaderIndexes.COLUMN_LIST_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(4160223223L), SubheaderIndexes.ROW_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(4143380214L), SubheaderIndexes.COLUMN_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-578721386864836608L), SubheaderIndexes.ROW_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-651061559686070272L), SubheaderIndexes.COLUMN_SIZE_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(71213169107795967L), SubheaderIndexes.SUBHEADER_COUNTS_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-144115188075855873L), SubheaderIndexes.COLUMN_TEXT_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-1L), SubheaderIndexes.COLUMN_NAME_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-216172782113783809L), SubheaderIndexes.COLUMN_ATTRIBUTES_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-73183493944770561L), SubheaderIndexes.FORMAT_AND_LABEL_SUBHEADER_INDEX);
//    tmpMap.put(Long.valueOf(-72057594037927937L), SubheaderIndexes.COLUMN_LIST_SUBHEADER_INDEX);
//    SUBHEADER_SIGNATURE_TO_INDEX = Collections.unmodifiableMap(tmpMap);
//
//    LITERALS_TO_DECOMPRESSOR.put("SASYZCRL", CharDecompressor.INSTANCE);
//    LITERALS_TO_DECOMPRESSOR.put("SASYZCR2", BinDecompressor.INSTANCE);
//  }
//
//  private final List<SubheaderPointer> currentPageDataSubheaderPointers = new ArrayList();
//  private final SasFileProperties sasFileProperties = new SasFileProperties();
//  private final List<String> columnsNamesStrings = new ArrayList();
//  private final List<String> columnsNamesList = new ArrayList();
//  private final List<Class<?>> columnsTypesList = new ArrayList();
//  private final List<Long> columnsDataOffset = new ArrayList();
//  private final List<Integer> columnsDataLength = new ArrayList();
//  private final List<Column> columns = new ArrayList();
//  private final Map<SubheaderIndexes, ProcessingSubheader> subheaderIndexToClass;
//  private String encoding = "ASCII";
//  private byte[] cachedPage;
//  private int currentPageType;
//  private int currentPageBlockCount;
//  private int currentPageSubheadersCount;
//  private int currentFilePosition;
//  private int currentColumnNumber;
//  private int currentRowInFileIndex;
//  private int currentRowOnPageIndex;
//  private Object[] currentRow;
//  private boolean eof;
//
//  private SasFileParser(Builder builder)
//  {
//    this.sasFileStream = new DataInputStream(builder.sasFileStream);
//    this.encoding = builder.encoding;
//    this.byteOutput = builder.byteOutput;
//
//    Map<SubheaderIndexes, ProcessingSubheader> tmpMap = new HashMap();
//    tmpMap.put(SubheaderIndexes.ROW_SIZE_SUBHEADER_INDEX, new RowSizeSubheader());
//    tmpMap.put(SubheaderIndexes.COLUMN_SIZE_SUBHEADER_INDEX, new ColumnSizeSubheader());
//    tmpMap.put(SubheaderIndexes.SUBHEADER_COUNTS_SUBHEADER_INDEX, new SubheaderCountsSubheader());
//    tmpMap.put(SubheaderIndexes.COLUMN_TEXT_SUBHEADER_INDEX, new ColumnTextSubheader());
//    tmpMap.put(SubheaderIndexes.COLUMN_NAME_SUBHEADER_INDEX, new ColumnNameSubheader());
//    tmpMap.put(SubheaderIndexes.COLUMN_ATTRIBUTES_SUBHEADER_INDEX, new ColumnAttributesSubheader());
//    tmpMap.put(SubheaderIndexes.FORMAT_AND_LABEL_SUBHEADER_INDEX, new FormatAndLabelSubheader());
//    tmpMap.put(SubheaderIndexes.COLUMN_LIST_SUBHEADER_INDEX, new ColumnListSubheader());
//    tmpMap.put(SubheaderIndexes.DATA_SUBHEADER_INDEX, new DataSubheader());
//    this.subheaderIndexToClass = Collections.unmodifiableMap(tmpMap);
//    try
//    {
//      getMetadataFromSasFile();
//    }
//    catch (IOException e)
//    {
//      LOGGER.error(e.getMessage(), e);
//    }
//  }
//
//  private void getMetadataFromSasFile()
//    throws IOException
//  {
//    boolean endOfMetadata = false;
//    processSasFileHeader();
//    this.cachedPage = new byte[this.sasFileProperties.getPageLength()];
//    while (!endOfMetadata)
//    {
//      try
//      {
//        this.sasFileStream.readFully(this.cachedPage, 0, this.sasFileProperties.getPageLength());
//      }
//      catch (EOFException ex)
//      {
//        this.eof = true;
//        break;
//      }
//      endOfMetadata = processSasFilePageMeta();
//    }
//  }
//
//  private void processSasFileHeader()
//    throws IOException
//  {
//    int align1 = 0;
//    int align2 = 0;
//
//    Long[] offsetForAlign = { Long.valueOf(32L), Long.valueOf(35L) };
//    Integer[] lengthForAlign = { Integer.valueOf(1), Integer.valueOf(1) };
//    List<byte[]> varsForAlign = getBytesFromFile(offsetForAlign, lengthForAlign);
//    if (((byte[])varsForAlign.get(0))[0] == 51)
//    {
//      align2 = 4;
//      this.sasFileProperties.setU64(true);
//    }
//    if (((byte[])varsForAlign.get(1))[0] == 51) {
//      align1 = 4;
//    }
//    int totalAlign = align1 + align2;
//
//    Long[] offset = { Long.valueOf(37L), Long.valueOf(92L), Long.valueOf(156L), Long.valueOf(164L + align1), Long.valueOf(172L + align1), Long.valueOf(196L + align1), Long.valueOf(200L + align1), Long.valueOf(204L + align1), Long.valueOf(216L + totalAlign), Long.valueOf(224L + totalAlign), Long.valueOf(240L + totalAlign), Long.valueOf(256L + totalAlign), Long.valueOf(272L + totalAlign) };
//
//    Integer[] length = { Integer.valueOf(1), Integer.valueOf(64), Integer.valueOf(8), Integer.valueOf(8), Integer.valueOf(8), Integer.valueOf(4), Integer.valueOf(4), Integer.valueOf(4 + align2), Integer.valueOf(8), Integer.valueOf(16), Integer.valueOf(16), Integer.valueOf(16), Integer.valueOf(16) };
//    List<byte[]> vars = getBytesFromFile(offset, length);
//
//    this.sasFileProperties.setEndianness(((byte[])vars.get(0))[0]);
//    this.sasFileProperties.setName(bytesToString((byte[])vars.get(1)).trim());
//    this.sasFileProperties.setFileType(bytesToString((byte[])vars.get(2)).trim());
//    this.sasFileProperties.setDateCreated(bytesToDateTime((byte[])vars.get(3)));
//    this.sasFileProperties.setDateModified(bytesToDateTime((byte[])vars.get(4)));
//    this.sasFileProperties.setHeaderLength(bytesToInt((byte[])vars.get(5)));
//    this.sasFileProperties.setPageLength(bytesToInt((byte[])vars.get(6)));
//    this.sasFileProperties.setPageCount(bytesToLong((byte[])vars.get(7)));
//    this.sasFileProperties.setSasRelease(bytesToString((byte[])vars.get(8)).trim());
//    this.sasFileProperties.setServerType(bytesToString((byte[])vars.get(9)).trim());
//    this.sasFileProperties.setOsType(bytesToString((byte[])vars.get(10)).trim());
//    if (((byte[])vars.get(12))[0] != 0) {
//      this.sasFileProperties.setOsName(bytesToString((byte[])vars.get(12)).trim());
//    } else {
//      this.sasFileProperties.setOsName(bytesToString((byte[])vars.get(11)).trim());
//    }
//    if (this.sasFileStream != null)
//    {
//      int bytesLeft = this.sasFileProperties.getHeaderLength() - this.currentFilePosition;
//
//      long actuallySkipped = 0L;
//      while (actuallySkipped < bytesLeft) {
//        actuallySkipped += this.sasFileStream.skip(bytesLeft - actuallySkipped);
//      }
//      this.currentFilePosition = 0;
//    }
//  }
//
//  private boolean processSasFilePageMeta()
//    throws IOException
//  {
//    int bitOffset = this.sasFileProperties.isU64() ? 32 : 16;
//
//    readPageHeader();
//    List<SubheaderPointer> subheaderPointers = new ArrayList();
//    if ((this.currentPageType == 0) || (this.currentPageType == 512)) {
//      processPageMetadata(bitOffset, subheaderPointers);
//    }
//    return (this.currentPageType == 256) || (this.currentPageType == 512) || (this.currentPageDataSubheaderPointers.size() != 0);
//  }
//
//  private void processPageMetadata(int bitOffset, List<SubheaderPointer> subheaderPointers)
//    throws IOException
//  {
//    subheaderPointers.clear();
//    for (int subheaderPointerIndex = 0; subheaderPointerIndex < this.currentPageSubheadersCount; subheaderPointerIndex++)
//    {
//      SubheaderPointer currentSubheaderPointer = processSubheaderPointers(bitOffset + 8L, subheaderPointerIndex);
//
//      subheaderPointers.add(currentSubheaderPointer);
//      if (currentSubheaderPointer.compression != 1)
//      {
//        long subheaderSignature = readSubheaderSignature(Long.valueOf(currentSubheaderPointer.offset));
//        SubheaderIndexes subheaderIndex = chooseSubheaderClass(subheaderSignature,
//          currentSubheaderPointer.compression, currentSubheaderPointer.type);
//        if (subheaderIndex != null)
//        {
//          if (subheaderIndex != SubheaderIndexes.DATA_SUBHEADER_INDEX)
//          {
//            LOGGER.debug("Subheader process function name: {}", subheaderIndex);
//            ((ProcessingSubheader)this.subheaderIndexToClass.get(subheaderIndex)).processSubheader(
//              ((SubheaderPointer)subheaderPointers.get(subheaderPointerIndex)).offset,
//              ((SubheaderPointer)subheaderPointers.get(subheaderPointerIndex)).length);
//          }
//          else
//          {
//            this.currentPageDataSubheaderPointers.add(subheaderPointers.get(subheaderPointerIndex));
//          }
//        }
//        else {
//          LOGGER.debug("Unknown subheader signature");
//        }
//      }
//    }
//  }
//
//  private long readSubheaderSignature(Long subheaderPointerOffset)
//    throws IOException
//  {
//    int intOrLongLength = this.sasFileProperties.isU64() ? 8 : 4;
//
//    Long[] subheaderOffsetMass = { subheaderPointerOffset };
//    Integer[] subheaderLengthMass = { Integer.valueOf(intOrLongLength) };
//    List<byte[]> subheaderSignatureMass = getBytesFromFile(subheaderOffsetMass, subheaderLengthMass);
//
//    return bytesToLong((byte[])subheaderSignatureMass.get(0));
//  }
//
//  private SubheaderIndexes chooseSubheaderClass(long subheaderSignature, int compression, int type)
//  {
//    SubheaderIndexes subheaderIndex = (SubheaderIndexes)SUBHEADER_SIGNATURE_TO_INDEX.get(Long.valueOf(subheaderSignature));
//    if ((this.sasFileProperties.isCompressed()) && (subheaderIndex == null) && ((compression == 4) || (compression == 0)) && (type == 1)) {
//      subheaderIndex = SubheaderIndexes.DATA_SUBHEADER_INDEX;
//    }
//    return subheaderIndex;
//  }
//
//  private SubheaderPointer processSubheaderPointers(long subheaderPointerOffset, int subheaderPointerIndex)
//    throws IOException
//  {
//    int intOrLongLength = this.sasFileProperties.isU64() ? 8 : 4;
//
//    int subheaderPointerLength = this.sasFileProperties.isU64() ? 24 : 12;
//
//    long totalOffset = subheaderPointerOffset + subheaderPointerLength * subheaderPointerIndex;
//
//    Long[] offset = { Long.valueOf(totalOffset), Long.valueOf(totalOffset + intOrLongLength), Long.valueOf(totalOffset + 2L * intOrLongLength), Long.valueOf(totalOffset + 2L * intOrLongLength + 1L) };
//    Integer[] length = { Integer.valueOf(intOrLongLength), Integer.valueOf(intOrLongLength), Integer.valueOf(1), Integer.valueOf(1) };
//    List<byte[]> vars = getBytesFromFile(offset, length);
//
//    long subheaderOffset = bytesToLong((byte[])vars.get(0));
//    long subheaderLength = bytesToLong((byte[])vars.get(1));
//    byte subheaderCompression = ((byte[])vars.get(2))[0];
//    byte subheaderType = ((byte[])vars.get(3))[0];
//
//    return new SubheaderPointer(subheaderOffset, subheaderLength, subheaderCompression, subheaderType);
//  }
//
//  private String findCompressionLiteral(String src)
//  {
//    if (src == null)
//    {
//      LOGGER.warn("Null provided as the file compression literal, assuming no compression");
//      return null;
//    }
//    for (String supported : LITERALS_TO_DECOMPRESSOR.keySet()) {
//      if (src.contains(supported)) {
//        return supported;
//      }
//    }
//    LOGGER.debug("No supported compression literal found, assuming no compression");
//    return null;
//  }
//
//  Object[] readNext()
//    throws IOException
//  {
//    if ((this.currentRowInFileIndex++ >= this.sasFileProperties.getRowCount()) || (this.eof)) {
//      return null;
//    }
//    int bitOffset = this.sasFileProperties.isU64() ? 32 : 16;
//    switch (this.currentPageType)
//    {
//    case 0:
//      SubheaderPointer currentSubheaderPointer = (SubheaderPointer)this.currentPageDataSubheaderPointers.get(this.currentRowOnPageIndex++);
//      ((ProcessingSubheader)this.subheaderIndexToClass.get(SubheaderIndexes.DATA_SUBHEADER_INDEX)).processSubheader(
//        currentSubheaderPointer.offset, currentSubheaderPointer.length);
//      if (this.currentRowOnPageIndex == this.currentPageDataSubheaderPointers.size())
//      {
//        readNextPage();
//        this.currentRowOnPageIndex = 0;
//      }
//      break;
//    case 512:
//      int subheaderPointerLength = this.sasFileProperties.isU64() ? 24 : 12;
//
//      int alignCorrection = (bitOffset + 8 + this.currentPageSubheadersCount * subheaderPointerLength) % 8;
//
//      this.currentRow = processByteArrayWithData(bitOffset + 8 + alignCorrection + this.currentPageSubheadersCount * subheaderPointerLength + this.currentRowOnPageIndex++ * this.sasFileProperties
//
//        .getRowLength(), this.sasFileProperties.getRowLength());
//      if (this.currentRowOnPageIndex == Math.min(this.sasFileProperties.getRowCount(), this.sasFileProperties
//        .getMixPageRowCount()))
//      {
//        readNextPage();
//        this.currentRowOnPageIndex = 0;
//      }
//      break;
//    case 256:
//      this.currentRow = processByteArrayWithData(bitOffset + 8 + this.currentRowOnPageIndex++ * this.sasFileProperties
//        .getRowLength(), this.sasFileProperties.getRowLength());
//      if (this.currentRowOnPageIndex == this.currentPageBlockCount)
//      {
//        readNextPage();
//        this.currentRowOnPageIndex = 0;
//      }
//      break;
//    }
//    return Arrays.copyOf(this.currentRow, this.currentRow.length);
//  }
//
//  private void readNextPage()
//    throws IOException
//  {
//    processNextPage();
//    while ((this.currentPageType != 0) && (this.currentPageType != 512) && (this.currentPageType != 256))
//    {
//      if (this.eof) {
//        return;
//      }
//      processNextPage();
//    }
//  }
//
//  private void processNextPage()
//    throws IOException
//  {
//    int bitOffset = this.sasFileProperties.isU64() ? 32 : 16;
//
//    this.currentPageDataSubheaderPointers.clear();
//    try
//    {
//      this.sasFileStream.readFully(this.cachedPage, 0, this.sasFileProperties.getPageLength());
//    }
//    catch (EOFException ex)
//    {
//      this.eof = true;
//      return;
//    }
//    readPageHeader();
//    if (this.currentPageType == 0)
//    {
//      List<SubheaderPointer> subheaderPointers = new ArrayList();
//      processPageMetadata(bitOffset, subheaderPointers);
//    }
//  }
//
//  private void readPageHeader()
//    throws IOException
//  {
//    int bitOffset = this.sasFileProperties.isU64() ? 32 : 16;
//
//    Long[] offset = { Long.valueOf(bitOffset + 0L), Long.valueOf(bitOffset + 2L), Long.valueOf(bitOffset + 4L) };
//    Integer[] length = { Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2) };
//
//    List<byte[]> vars = getBytesFromFile(offset, length);
//
//    this.currentPageType = bytesToShort((byte[])vars.get(0));
//    LOGGER.debug("Page type: {}", Integer.valueOf(this.currentPageType));
//    this.currentPageBlockCount = bytesToShort((byte[])vars.get(1));
//    LOGGER.debug("Block count: {}", Integer.valueOf(this.currentPageBlockCount));
//    this.currentPageSubheadersCount = bytesToShort((byte[])vars.get(2));
//    LOGGER.debug("Subheader count: {}", Integer.valueOf(this.currentPageSubheadersCount));
//  }
//
//  private Object[] processByteArrayWithData(long rowOffset, long rowLength)
//  {
//    Object[] rowElements = new Object[(int)this.sasFileProperties.getColumnsCount()];
//    int offset;
//    byte[] source;
//    int offset;
//    if ((this.sasFileProperties.isCompressed()) && (rowLength < this.sasFileProperties.getRowLength()))
//    {
//      Decompressor decompressor = (Decompressor)LITERALS_TO_DECOMPRESSOR.get(this.sasFileProperties.getCompressionMethod());
//      byte[] source = decompressor.decompressRow((int)rowOffset, (int)rowLength,
//        (int)this.sasFileProperties.getRowLength(), this.cachedPage);
//      offset = 0;
//    }
//    else
//    {
//      source = this.cachedPage;
//      offset = (int)rowOffset;
//    }
//    for (int currentColumnIndex = 0; (
//          currentColumnIndex < this.sasFileProperties.getColumnsCount()) && (((Integer)this.columnsDataLength.get(currentColumnIndex)).intValue() != 0); currentColumnIndex++)
//    {
//      int length = ((Integer)this.columnsDataLength.get(currentColumnIndex)).intValue();
//      if (((Column)this.columns.get(currentColumnIndex)).getType() == Number.class)
//      {
//        byte[] temp = Arrays.copyOfRange(source, offset + (int)((Long)this.columnsDataOffset.get(currentColumnIndex)).longValue(), offset +
//          (int)((Long)this.columnsDataOffset.get(currentColumnIndex)).longValue() + length);
//        if (((Integer)this.columnsDataLength.get(currentColumnIndex)).intValue() <= 2) {
//          rowElements[currentColumnIndex] = Integer.valueOf(bytesToShort(temp));
//        } else if (((Column)this.columns.get(currentColumnIndex)).getFormat().isEmpty()) {
//          rowElements[currentColumnIndex] = convertByteArrayToNumber(temp);
//        } else if (SasFileConstants.DATE_TIME_FORMAT_STRINGS.contains(
//          ((Column)this.columns.get(currentColumnIndex)).getFormat())) {
//          rowElements[currentColumnIndex] = bytesToDateTime(temp);
//        } else if (SasFileConstants.DATE_FORMAT_STRINGS.contains(
//          ((Column)this.columns.get(currentColumnIndex)).getFormat())) {
//          rowElements[currentColumnIndex] = bytesToDate(temp);
//        } else {
//          rowElements[currentColumnIndex] = convertByteArrayToNumber(temp);
//        }
//      }
//      else
//      {
//        byte[] bytes = trimBytesArray(source, offset +
//          ((Long)this.columnsDataOffset.get(currentColumnIndex)).intValue(), length);
//        if (this.byteOutput.booleanValue()) {
//          rowElements[currentColumnIndex] = bytes;
//        } else {
//          try
//          {
//            rowElements[currentColumnIndex] = (bytes == null ? null : new String(bytes, this.encoding));
//          }
//          catch (UnsupportedEncodingException e)
//          {
//            LOGGER.error(e.getMessage(), e);
//          }
//        }
//      }
//    }
//    return rowElements;
//  }
//
//  private List<byte[]> getBytesFromFile(Long[] offset, Integer[] length)
//    throws IOException
//  {
//    List<byte[]> vars = new ArrayList();
//    if (this.cachedPage == null) {
//      for (int i = 0; i < offset.length; i++)
//      {
//        byte[] temp = new byte[length[i].intValue()];
//        long actuallySkipped = 0L;
//        while (actuallySkipped < offset[i].longValue() - this.currentFilePosition) {
//          actuallySkipped += this.sasFileStream.skip(offset[i].longValue() - this.currentFilePosition - actuallySkipped);
//        }
//        try
//        {
//          this.sasFileStream.readFully(temp, 0, length[i].intValue());
//        }
//        catch (EOFException e)
//        {
//          this.eof = true;
//        }
//        this.currentFilePosition = ((int)offset[i].longValue() + length[i].intValue());
//        vars.add(temp);
//      }
//    } else {
//      for (int i = 0; i < offset.length; i++) {
//        vars.add(Arrays.copyOfRange(this.cachedPage, (int)offset[i].longValue(), (int)offset[i].longValue() + length[i].intValue()));
//      }
//    }
//    return vars;
//  }
//
//  private long correctLongProcess(ByteBuffer byteBuffer)
//  {
//    if (this.sasFileProperties.isU64()) {
//      return byteBuffer.getLong();
//    }
//    return byteBuffer.getInt();
//  }
//
//  private ByteBuffer byteArrayToByteBuffer(byte[] data)
//  {
//    ByteBuffer byteBuffer = ByteBuffer.wrap(data);
//    if (this.sasFileProperties.getEndianness() == 0) {
//      return byteBuffer;
//    }
//    return byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
//  }
//
//  private Object convertByteArrayToNumber(byte[] mass)
//  {
//    ByteBuffer original = byteArrayToByteBuffer(mass);
//    if (mass.length < 8)
//    {
//      ByteBuffer byteBuffer = ByteBuffer.allocate(8);
//      if (this.sasFileProperties.getEndianness() == 1) {
//        byteBuffer.position(8 - mass.length);
//      }
//      byteBuffer.put(original);
//      byteBuffer.order(original.order());
//      byteBuffer.position(0);
//      original = byteBuffer;
//    }
//    double resultDouble = original.getDouble();
//    original.clear();
//    if ((Double.isNaN(resultDouble)) || ((resultDouble < 1.0E-300D) && (resultDouble > 0.0D))) {
//      return null;
//    }
//    long resultLong = Math.round(resultDouble);
//    if (Math.abs(resultDouble - resultLong) >= 1.0E-14D) {
//      return Double.valueOf(resultDouble);
//    }
//    return Long.valueOf(resultLong);
//  }
//
//  private int bytesToShort(byte[] bytes)
//  {
//    return byteArrayToByteBuffer(bytes).getShort();
//  }
//
//  private int bytesToInt(byte[] bytes)
//  {
//    return byteArrayToByteBuffer(bytes).getInt();
//  }
//
//  private long bytesToLong(byte[] bytes)
//  {
//    return correctLongProcess(byteArrayToByteBuffer(bytes));
//  }
//
//  private String bytesToString(byte[] bytes)
//  {
//    return new String(bytes);
//  }
//
//  private Date bytesToDateTime(byte[] bytes)
//  {
//    double doubleSeconds = byteArrayToByteBuffer(bytes).getDouble();
//    return new Date((long) ((doubleSeconds - SasFileConstants.START_DATES_SECONDS_DIFFERENCE) * SasFileConstants.MILLISECONDS_IN_SECONDS));
//  }
//
//  private Date bytesToDate(byte[] bytes)
//  {
//    double doubleDays;
//    double doubleDays;
//    if (bytes.length == 4)
//    {
//      byte[] ba = new byte[8];
//      ba[0] = bytes[0];
//      ba[1] = bytes[1];
//      ba[2] = bytes[2];
//      ba[3] = bytes[3];
//      ba[4] = 0;
//      ba[5] = 0;
//      ba[6] = 0;
//      ba[7] = 0;
//      ByteBuffer bb = byteArrayToByteBuffer(ba);
//      doubleDays = bb.getDouble();
//    }
//    else
//    {
//      doubleDays = byteArrayToByteBuffer(bytes).getDouble();
//    }
//    return Double.isNaN(doubleDays) ? null : new Date(((doubleDays - 3653.0D) * 60.0D * 60.0D * 24.0D * 1000.0D));
//  }
//
//  private byte[] trimBytesArray(byte[] source, int offset, int length)
//  {
//    for (int lengthFromBegin = offset + length; lengthFromBegin > offset; lengthFromBegin--) {
//      if ((source[(lengthFromBegin - 1)] != 32) && (source[(lengthFromBegin - 1)] != 0) && (source[(lengthFromBegin - 1)] != 9)) {
//        break;
//      }
//    }
//    if (lengthFromBegin - offset != 0) {
//      return Arrays.copyOfRange(source, offset, lengthFromBegin);
//    }
//    return null;
//  }
//
//  List<Column> getColumns()
//  {
//    return this.columns;
//  }
//
//  SasFileProperties getSasFileProperties()
//  {
//    return this.sasFileProperties;
//  }
//
//  private static enum SubheaderIndexes
//  {
//    ROW_SIZE_SUBHEADER_INDEX,  COLUMN_SIZE_SUBHEADER_INDEX,  SUBHEADER_COUNTS_SUBHEADER_INDEX,  COLUMN_TEXT_SUBHEADER_INDEX,  COLUMN_NAME_SUBHEADER_INDEX,  COLUMN_ATTRIBUTES_SUBHEADER_INDEX,  FORMAT_AND_LABEL_SUBHEADER_INDEX,  COLUMN_LIST_SUBHEADER_INDEX,  DATA_SUBHEADER_INDEX;
//
//    private SubheaderIndexes() {}
//  }
//
//  private static abstract interface ProcessingSubheader
//  {
//    public abstract void processSubheader(long paramLong1, long paramLong2)
//      throws IOException;
//  }
//
//  static class Builder
//  {
//    private InputStream sasFileStream;
//    private String encoding = "ASCII";
//    private Boolean byteOutput = Boolean.valueOf(false);
//
//    Builder sasFileStream(InputStream val)
//    {
//      this.sasFileStream = val;
//      return this;
//    }
//
//    Builder encoding(String val)
//    {
//      this.encoding = val;
//      return this;
//    }
//
//    Builder byteOutput(Boolean val)
//    {
//      this.byteOutput = val;
//      return this;
//    }
//
//    SasFileParser build()
//    {
//      return new SasFileParser(this, null);
//    }
//  }
//
//  class SubheaderPointer
//  {
//    private final long offset;
//    private final long length;
//    private final byte compression;
//    private final byte type;
//
//    SubheaderPointer(long offset, long length, byte compression, byte type)
//    {
//      this.offset = offset;
//      this.length = length;
//      this.compression = compression;
//      this.type = type;
//    }
//  }
//
//  class RowSizeSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    RowSizeSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      Long[] offset = { Long.valueOf(subheaderOffset + 5 * intOrLongLength), Long.valueOf(subheaderOffset + 6 * intOrLongLength), Long.valueOf(subheaderOffset + 15 * intOrLongLength) };
//      Integer[] length = { Integer.valueOf(intOrLongLength), Integer.valueOf(intOrLongLength), Integer.valueOf(intOrLongLength) };
//      List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//      if (SasFileParser.this.sasFileProperties.getRowLength() == 0L) {
//        SasFileParser.this.sasFileProperties.setRowLength(SasFileParser.this.bytesToLong((byte[])vars.get(0)));
//      }
//      if (SasFileParser.this.sasFileProperties.getRowCount() == 0L) {
//        SasFileParser.this.sasFileProperties.setRowCount(SasFileParser.this.bytesToLong((byte[])vars.get(1)));
//      }
//      if (SasFileParser.this.sasFileProperties.getMixPageRowCount() == 0L) {
//        SasFileParser.this.sasFileProperties.setMixPageRowCount(SasFileParser.this.bytesToLong((byte[])vars.get(2)));
//      }
//    }
//  }
//
//  class ColumnSizeSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    ColumnSizeSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      Long[] offset = { Long.valueOf(subheaderOffset + intOrLongLength) };
//      Integer[] length = { Integer.valueOf(intOrLongLength) };
//      List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//
//      SasFileParser.this.sasFileProperties.setColumnsCount(SasFileParser.this.bytesToLong((byte[])vars.get(0)));
//    }
//  }
//
//  class SubheaderCountsSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    SubheaderCountsSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {}
//  }
//
//  class ColumnTextSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    ColumnTextSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      Long[] offset = { Long.valueOf(subheaderOffset + intOrLongLength) };
//      Integer[] length = { Integer.valueOf(2) };
//      List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//      int textBlockSize = SasFileParser.this.byteArrayToByteBuffer((byte[])vars.get(0)).getShort();
//
//      offset[0] = Long.valueOf(subheaderOffset + intOrLongLength);
//      length[0] = Integer.valueOf(textBlockSize);
//      vars = SasFileParser.this.getBytesFromFile(offset, length);
//
//      SasFileParser.this.columnsNamesStrings.add(SasFileParser.this.bytesToString((byte[])vars.get(0)));
//      if (SasFileParser.this.columnsNamesStrings.size() == 1)
//      {
//        String columnName = (String)SasFileParser.this.columnsNamesStrings.get(0);
//        String compessionLiteral = SasFileParser.this.findCompressionLiteral(columnName);
//        SasFileParser.this.sasFileProperties.setCompressionMethod(compessionLiteral);
//      }
//    }
//  }
//
//  class ColumnNameSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    ColumnNameSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      long columnNamePointersCount = (subheaderLength - 2 * intOrLongLength - 12L) / 8L;
//      for (int i = 0; i < columnNamePointersCount; i++)
//      {
//        Long[] offset = { Long.valueOf(subheaderOffset + intOrLongLength + 8 * (i + 1) + 0L), Long.valueOf(subheaderOffset + intOrLongLength + 8 * (i + 1) + 2L), Long.valueOf(subheaderOffset + intOrLongLength + 8 * (i + 1) + 4L) };
//
//        Integer[] length = { Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2) };
//        List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//
//        int textSubheaderIndex = SasFileParser.this.bytesToShort((byte[])vars.get(0));
//        int columnNameOffset = SasFileParser.this.bytesToShort((byte[])vars.get(1));
//        int columnNameLength = SasFileParser.this.bytesToShort((byte[])vars.get(2));
//        SasFileParser.this.columnsNamesList.add(((String)SasFileParser.this.columnsNamesStrings.get(textSubheaderIndex)).substring(columnNameOffset, columnNameOffset + columnNameLength)
//          .intern());
//      }
//    }
//  }
//
//  class ColumnAttributesSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    ColumnAttributesSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      long columnAttributesVectorsCount = (subheaderLength - 2 * intOrLongLength - 12L) / (intOrLongLength + 8);
//      for (int i = 0; i < columnAttributesVectorsCount; i++)
//      {
//        Long[] offset = { Long.valueOf(subheaderOffset + intOrLongLength + 8L + i * (intOrLongLength + 8)), Long.valueOf(subheaderOffset + 2 * intOrLongLength + 8L + i * (intOrLongLength + 8)), Long.valueOf(subheaderOffset + 2 * intOrLongLength + 14L + i * (intOrLongLength + 8)) };
//
//        Integer[] length = { Integer.valueOf(intOrLongLength), Integer.valueOf(4), Integer.valueOf(1) };
//
//        List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//
//        SasFileParser.this.columnsDataOffset.add(Long.valueOf(SasFileParser.this.bytesToLong((byte[])vars.get(0))));
//        SasFileParser.this.columnsDataLength.add(Integer.valueOf(SasFileParser.this.bytesToInt((byte[])vars.get(1))));
//        SasFileParser.this.columnsTypesList.add(((byte[])vars.get(2))[0] == 1 ? Number.class : String.class);
//      }
//    }
//  }
//
//  class FormatAndLabelSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    FormatAndLabelSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      int intOrLongLength = SasFileParser.this.sasFileProperties.isU64() ? 8 : 4;
//
//      Long[] offset = { Long.valueOf(subheaderOffset + 22L + 3 * intOrLongLength), Long.valueOf(subheaderOffset + 24L + 3 * intOrLongLength), Long.valueOf(subheaderOffset + 26L + 3 * intOrLongLength), Long.valueOf(subheaderOffset + 28L + 3 * intOrLongLength), Long.valueOf(subheaderOffset + 30L + 3 * intOrLongLength), Long.valueOf(subheaderOffset + 32L + 3 * intOrLongLength) };
//
//      Integer[] length = { Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2), Integer.valueOf(2) };
//      List<byte[]> vars = SasFileParser.this.getBytesFromFile(offset, length);
//
//      int textSubheaderIndexForFormat = Math.min(SasFileParser.this.bytesToShort((byte[])vars.get(0)), SasFileParser.this.columnsNamesStrings.size() - 1);
//      int columnFormatOffset = SasFileParser.this.bytesToShort((byte[])vars.get(1));
//      int columnFormatLength = SasFileParser.this.bytesToShort((byte[])vars.get(2));
//
//      int textSubheaderIndexForLabel = Math.min(SasFileParser.this.bytesToShort((byte[])vars.get(3)), SasFileParser.this.columnsNamesStrings.size() - 1);
//      int columnLabelOffset = SasFileParser.this.bytesToShort((byte[])vars.get(4));
//      int columnLabelLength = SasFileParser.this.bytesToShort((byte[])vars.get(5));
//
//      String columnLabel = ((String)SasFileParser.this.columnsNamesStrings.get(textSubheaderIndexForLabel)).substring(columnLabelOffset, columnLabelOffset + columnLabelLength).intern();
//
//      String columnFormat = ((String)SasFileParser.this.columnsNamesStrings.get(textSubheaderIndexForFormat)).substring(columnFormatOffset, columnFormatOffset + columnFormatLength).intern();
//      SasFileParser.LOGGER.debug("Column format: {}", columnFormat);
//      SasFileParser.this.columns.add(new Column(SasFileParser.this.currentColumnNumber + 1, (String)SasFileParser.this.columnsNamesList.get(SasFileParser.this.columns.size()), columnLabel, columnFormat,
//        (Class)SasFileParser.this.columnsTypesList.get(SasFileParser.this.columns.size()),
//        ((Integer)SasFileParser.this.columnsDataLength.get(SasFileParser.access$2208(SasFileParser.this))).intValue()));
//    }
//  }
//
//  class ColumnListSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    ColumnListSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {}
//  }
//
//  class DataSubheader
//    implements SasFileParser.ProcessingSubheader
//  {
//    DataSubheader() {}
//
//    public void processSubheader(long subheaderOffset, long subheaderLength)
//      throws IOException
//    {
//      SasFileParser.this.currentRow = SasFileParser.this.processByteArrayWithData(subheaderOffset, subheaderLength);
//    }
//  }
//}
