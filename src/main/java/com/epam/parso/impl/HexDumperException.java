package com.epam.parso.impl;

public class HexDumperException extends RuntimeException { 
	 
    public HexDumperException(Throwable t) { 
        super(t); 
    } 
     
}