package com.epam.parso.impl;

import com.epam.parso.CSVMetadataWriter;
import com.epam.parso.Column;
import com.epam.parso.SasFileProperties;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class CSVMetadataWriterImpl extends AbstractCSVWriter
  implements CSVMetadataWriter
{
  private static final String COLUMN_HEADING_ID = "Number";
  private static final String COLUMN_HEADING_NAME = "Name";
  private static final String COLUMN_HEADING_TYPE = "Type";
  private static final String COLUMN_HEADING_DATA_LENGTH = "Data length";
  private static final String COLUMN_HEADING_FORMAT = "Format";
  private static final String COLUMN_HEADING_LABEL = "Label";
  private static final String JAVA_NUMBER_CLASS_NAME = "java.lang.Number";
  private static final String JAVA_STRING_CLASS_NAME = "java.lang.String";
  private static final String OUTPUT_NUMBER_TYPE_NAME = "Numeric";
  private static final String OUTPUT_STRING_TYPE_NAME = "Character";

  public CSVMetadataWriterImpl(Writer writer)
  {
    super(writer);
  }

  public CSVMetadataWriterImpl(Writer writer, String delimiter)
  {
    super(writer, delimiter);
  }

  public CSVMetadataWriterImpl(Writer writer, String delimiter, String endline)
  {
    super(writer, delimiter, endline);
  }

  public void writeMetadata(List<Column> columns)
    throws IOException
  {
    Writer writer = getWriter();
    String delimiter = getDelimiter();
    String endline = getEndline();

    writer.write("Number");
    writer.write(delimiter);
    writer.write("Name");
    writer.write(delimiter);
    writer.write("Type");
    writer.write(delimiter);
    writer.write("Data length");
    writer.write(delimiter);
    writer.write("Format");
    writer.write(delimiter);
    writer.write("Label");
    writer.write(endline);
    for (Column column : columns) {
      writer.write(String.valueOf(column.getId()));
      writer.write(delimiter);
      checkSurroundByQuotesAndWrite(writer, delimiter, column.getName());
      writer.write(delimiter);
      writer.write(column.getType().getName().replace("java.lang.Number", "Numeric").replace("java.lang.String", "Character"));

      writer.write(delimiter);
      writer.write(String.valueOf(column.getLength()));
      writer.write(delimiter);
      if (!column.getFormat().isEmpty()) {
        checkSurroundByQuotesAndWrite(writer, delimiter, column.getFormat() + ".");
      }
      writer.write(delimiter);
      checkSurroundByQuotesAndWrite(writer, delimiter, column.getLabel());
      writer.write(endline);
    }
    writer.flush();
  }

  public void writeSasFileProperties(SasFileProperties sasFileProperties)
    throws IOException
  {
    constructPropertiesString("Bitness: ", sasFileProperties.isU64() ? "x64" : "x86");
    constructPropertiesString("Compressed: ", sasFileProperties.getCompressionMethod());
    constructPropertiesString("Endianness: ", sasFileProperties.getEndianness() == 1 ? "LITTLE_ENDIANNESS" : "BIG_ENDIANNESS");

    constructPropertiesString("Name: ", sasFileProperties.getName());
    constructPropertiesString("File type: ", sasFileProperties.getFileType());
    constructPropertiesString("Date created: ", sasFileProperties.getDateCreated());
    constructPropertiesString("Date modified: ", sasFileProperties.getDateModified());
    constructPropertiesString("SAS release: ", sasFileProperties.getSasRelease());
    constructPropertiesString("SAS server type: ", sasFileProperties.getServerType());
    constructPropertiesString("OS name: ", sasFileProperties.getOsName());
    constructPropertiesString("OS type: ", sasFileProperties.getOsType());
    constructPropertiesString("Header Length: ", Integer.valueOf(sasFileProperties.getHeaderLength()));
    constructPropertiesString("Page Length: ", Integer.valueOf(sasFileProperties.getPageLength()));
    constructPropertiesString("Page Count: ", Long.valueOf(sasFileProperties.getPageCount()));
    constructPropertiesString("Row Length: ", Long.valueOf(sasFileProperties.getRowLength()));
    constructPropertiesString("Row Count: ", Long.valueOf(sasFileProperties.getRowCount()));
    constructPropertiesString("Mix Page Row Count: ", Long.valueOf(sasFileProperties.getMixPageRowCount()));
    constructPropertiesString("Columns Count: ", Long.valueOf(sasFileProperties.getColumnsCount()));
    getWriter().flush();
  }

  private void constructPropertiesString(String propertyName, Object property)
    throws IOException
  {
    getWriter().write(propertyName + String.valueOf(property) + "\n");
  }
}