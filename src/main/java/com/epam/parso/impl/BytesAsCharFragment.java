package com.epam.parso.impl;


import java.nio.ByteBuffer; 
import java.nio.CharBuffer; 
import java.nio.charset.Charset; 

public class BytesAsCharFragment implements DumpFragment { 

   private static final char[] asc = new char[256]; 

   static { 
       Charset charset = Charset.forName("ASCII"); 
       byte[] bytes = new byte[256]; 
       for (int i = 0; i <= 255; i++) { 
           asc[i] = '.'; 
           bytes[i] = (byte) (0xff & i); 
       } 
       CharBuffer buffer = charset.decode(ByteBuffer.wrap(bytes)); 
       for (int i = 0x20; i <= 0x7E; i++) { 
           asc[i] = buffer.get(i); 
       } 
   } 

   public int getSize(int numberOfBytes) { 
       return numberOfBytes; 
   } 

   public void dump(long lineNumber, byte[] buffer, int length, HexDumpTarget out) throws HexDumperException { 
       for (int i = 0; i < length; i++) { 
           out.writeText(asc[0xff & buffer[i]]); 
       } 
       for (int i = length; i < buffer.length; i++) { 
           out.writeText(' '); 
       } 
   } 
}
