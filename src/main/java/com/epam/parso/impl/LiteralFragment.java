package com.epam.parso.impl;

public class LiteralFragment implements DumpFragment { 
	 
    private final String literal; 
 
    /**
     * Constructs a new instance, accepting the literal fragment to be included when this fragment renders itself. 
     * 
     * @param literal 
     */ 
    public LiteralFragment(String literal) { 
        this.literal = literal; 
    } 
 
    public int getSize(int numberOfBytes) { 
        return literal.length(); 
    } 
 
    public void dump(long lineNumber, byte[] buffer, int length, HexDumpTarget out) throws HexDumperException { 
        out.writeText(literal); 
    } 
}
