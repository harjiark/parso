package com.epam.parso.impl;

import com.epam.parso.Column;
import com.epam.parso.SasFileProperties;
import com.epam.parso.SasFileReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by harji on 03/04/17.
 */
public class MainTestParso {
    public static void main(String[] args) throws FileNotFoundException {
        SasFileReader sasFileReader;

        FileInputStream is = new FileInputStream("/home/harji/Workspace/Project/lsproj/parso-2.0/src/test/resources/sas7bdat/test_csv.sas7bdat");
        sasFileReader = new SasFileReaderImpl(is);

        SasFileProperties sasprop = sasFileReader.getSasFileProperties();
        long columnSize = sasprop.getColumnsCount();
        long rowCount = sasprop.getRowCount();
        long rowLenght = sasprop.getRowLength();

        System.out.println("byte arab = " + ((byte) 0xC3));
        System.out.println("rowCount = " + rowCount);
        System.out.println("rowLenght = " + rowLenght);
        List<Column> columnList = sasFileReader.getColumns();
        List<String> DATE_TIME_FORMAT_STRINGS = Arrays.asList(new String[]{"DATETIME", "TIME","DATE9","DATEAMPM22"});
        List<String> DATE_FORMAT_STRINGS = Arrays.asList(new String[]{"YYMMDD", "MMDDYY", "DDMMYY", "DATE", "MMDDYY10","MMDDYY8"});

        for (Column col : columnList) {
            int id = col.getId();
            String variable = (col.getName() == null ? "" : col.getName());
            String type = col.getType().getSimpleName();
            int length = col.getLength();

            String format = col.getFormat();//(col.getFormat() == null ? "" : col.getFormat());
            String label = (col.getLabel() == null ? "" : col.getLabel());
            String typeByFormat = "";
            if(type.contains("Number")){
                if(format.matches("DOLLAR($|[0-9]{1,2}.[0-9]{1,2})")||format.matches("D($|[0-9]{1,2}.[0-9]{1,2})")){
                    typeByFormat = "Double";
                }else if (format.matches("I($|[0-9]{1,2}\\.)")) {
                    typeByFormat = "Integer";
                } else if (format.matches("L($|[0-9]{1,2}\\.)")) {
                    typeByFormat = "Long";
                }else if ((DATE_FORMAT_STRINGS.contains(format) | DATE_TIME_FORMAT_STRINGS.contains(format))) {
                    typeByFormat = "Datetime";
                }
            }else{
                typeByFormat = "String";
            }
            //my change the code here
//            if(format.trim().matches(currencyRegex)){
//                type = "double";
//            }
            String informat = " ";
            String all = id + "|" + variable + "|" + type + "|" + length + "|" + format + "|" + label+"|"+typeByFormat;
            System.out.println(all);
        }

    }
}
