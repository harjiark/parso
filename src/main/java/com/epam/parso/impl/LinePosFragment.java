package com.epam.parso.impl;

public class LinePosFragment implements DumpFragment { 
	 
    private final int size; 
 
    /**
     * Constructs a new instance, accepting the number of character positions reserved for the line number. 
     * 
     * @param size 
     */ 
    public LinePosFragment(int size) { 
        this.size = size; 
    } 
 
    public int getSize(int numberOfBytes) { 
        return size; 
    } 
 
    public void dump(long lineNumber, byte[] buffer, int length, HexDumpTarget out) throws HexDumperException { 
        String position = Long.toHexString(lineNumber); 
        if (position.length() > size) { 
            position = position.substring(position.length() - size); 
        } else { 
            for (int i = position.length(); i < size; i++) { 
                out.writeText('0'); 
            } 
        } 
        out.writeText(position); 
    } 
}
