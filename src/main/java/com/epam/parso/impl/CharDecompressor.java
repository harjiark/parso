package com.epam.parso.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class CharDecompressor
  implements Decompressor
{
  static final CharDecompressor INSTANCE = new CharDecompressor();

  private static final Logger LOGGER = LoggerFactory.getLogger(CharDecompressor.class);

  public byte[] decompressRow(int offset, int length, int resultLength, byte[] page)
  {
    byte[] resultByteArray = new byte[resultLength];
    int currentResultArrayIndex = 0;
    int currentByteIndex = 0;
    while (currentByteIndex < length) {
      int controlByte = page[(offset + currentByteIndex)] & 0xF0;
      int endOfFirstByte = page[(offset + currentByteIndex)] & 0xF;

      switch (controlByte) {
      case 0:
      case 16:
      case 32:
      case 48:
        if (currentByteIndex == length - 1) break;
        int countOfBytesToCopy = (page[(offset + currentByteIndex + 1)] & 0xFF) + 64 + page[(offset + currentByteIndex)] * 256;

        System.arraycopy(page, offset + currentByteIndex + 2, resultByteArray, currentResultArrayIndex, countOfBytesToCopy);

        currentByteIndex += countOfBytesToCopy + 1;
        currentResultArrayIndex += countOfBytesToCopy; break;
      case 64:
        int copyCounter = endOfFirstByte * 16 + (page[(offset + currentByteIndex + 1)] & 0xFF);
        for (int i = 0; i < copyCounter + 18; i++) {
          resultByteArray[(currentResultArrayIndex++)] = page[(offset + currentByteIndex + 2)];
        }
        currentByteIndex += 2;
        break;
      case 96:
        for (int i = 0; i < endOfFirstByte * 256 + (page[(offset + currentByteIndex + 1)] & 0xFF) + 17; i++) {
          resultByteArray[(currentResultArrayIndex++)] = 32;
        }
        currentByteIndex++;
        break;
      case 112:
        for (int i = 0; i < endOfFirstByte * 256 + (page[(offset + currentByteIndex + 1)] & 0xFF) + 17; i++) {
          resultByteArray[(currentResultArrayIndex++)] = 0;
        }
        currentByteIndex++;
        break;
      case 128:
      case 144:
      case 160:
      case 176:
        countOfBytesToCopy = Math.min(endOfFirstByte + 1 + (controlByte - 128), length - (currentByteIndex + 1));

        System.arraycopy(page, offset + currentByteIndex + 1, resultByteArray, currentResultArrayIndex, countOfBytesToCopy);

        currentByteIndex += countOfBytesToCopy;
        currentResultArrayIndex += countOfBytesToCopy;
        break;
      case 192:
        for (int i = 0; i < endOfFirstByte + 3; i++) {
          resultByteArray[(currentResultArrayIndex++)] = page[(offset + currentByteIndex + 1)];
        }
        currentByteIndex++;
        break;
      case 208:
        for (int i = 0; i < endOfFirstByte + 2; i++) {
          resultByteArray[(currentResultArrayIndex++)] = 64;
        }
        break;
      case 224:
        for (int i = 0; i < endOfFirstByte + 2; i++) {
          resultByteArray[(currentResultArrayIndex++)] = 32;
        }
        break;
      case 240:
        for (int i = 0; i < endOfFirstByte + 2; i++) {
          resultByteArray[(currentResultArrayIndex++)] = 0;
        }
        break;
      default:
        LOGGER.error("Error control byte: {}", Integer.valueOf(controlByte));
      }

      currentByteIndex++;
    }

    return resultByteArray;
  }
}