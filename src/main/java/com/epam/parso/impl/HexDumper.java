package com.epam.parso.impl;

import java.io.IOException; 
import java.nio.BufferUnderflowException; 
import java.nio.ByteBuffer; 
 
/**
 * A utility for generating hexdumps. The {@link org.codehaus.preon.hex.HexDumper} is quite a flexible class. It 
 * basically allows itself to be configured with any combination of {link DumpFragment}s. Each {@link 
 * org.codehaus.preon.hex.DumpFragment} represents a part of the line getting generated. 
 */ 
public class HexDumper { 
 
    /**
     * The number of bytes that will be fed to every line. 
     */ 
    private final int bytesPerLine; 
 
    /**
     * The fragments responsible for rendering the output. 
     */ 
    private final DumpFragment[] fragments; 
 
    /**
     * Constructs a new instance, accepting the number of bytes to printed on every line, and the fragments that should 
     * be included in every line. 
     * 
     * @param bytesPerLine 
     * @param fragments 
     */ 
    public HexDumper(int bytesPerLine, DumpFragment... fragments) { 
        this.bytesPerLine = bytesPerLine; 
        this.fragments = fragments; 
    } 
 
    public long dump(ByteBuffer in, Appendable out) throws HexDumperException { 
        return dump(in, new AppendableHexDumpTarget(out));         
    } 
 
    /**
     * Dumps a representation of the {@link ByteBuffer} to the given output object. 
     * 
     * @param in  The in containing the bytes. 
     * @param out The object receiving the output. 
     * @return The number of <em>bytes</em> generated 
     * @throws IOException If the operation fails while writing. 
     */ 
    public long dump(ByteBuffer in, HexDumpTarget out) throws HexDumperException { 
        long lineNumber = 0; 
        byte[] buffer = new byte[this.bytesPerLine]; 
        int written = 0; 
        in.rewind(); 
        while (in.hasRemaining()) { 
            written = fillBuffer(in, buffer); 
            for (DumpFragment fragment : fragments) { 
                fragment.dump(lineNumber, buffer, written, out); 
            } 
            lineNumber += 1; 
        } 
        return lineNumber * bytesPerLine + written; 
    } 
 
    /**
     * Fills a buffer with {@link #bytesPerLine} from a {@link ByteBuffer} passed in. 
     * 
     * @param in 
     * @param buffer 
     * @return The number of bytes pushed into the buffer. (Might be less than {@link #bytesPerLine} in case of a buffer 
     *         underflow. 
     */ 
    private int fillBuffer(ByteBuffer in, byte[] buffer) { 
        try { 
            in.get(buffer, 0, bytesPerLine); 
            return bytesPerLine; 
        } catch (BufferUnderflowException bue) { 
            int i = 0; 
            while (in.hasRemaining()) { 
                buffer[i++] = in.get(); 
            } 
            return i; 
        } 
    } 
 
}