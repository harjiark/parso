package com.epam.parso.impl;

public interface DumpFragment { 
	 
    /**
     * The number of characters this fragment will occupy, as a function of the number of bytes that <em>might</em> be 
     * presented. 
     * 
     * @return The number of characters this fragment will occupy. 
     */ 
    int getSize(int numberOfBytes); 
 
    /**
     * Dumps the contents of this fragment to <code>out</code>. 
     * 
     * @param lineNumber The line number. 
     * @param buffer     The bytes to be rendered. (Assume that the number of byt   es normally to be printed to every line
     *                   is the length of this buffer. 
     * @param out The object receiving output. 
     */ 
    void dump(long lineNumber, byte[] buffer, int length, HexDumpTarget out) throws HexDumperException; 
 
}