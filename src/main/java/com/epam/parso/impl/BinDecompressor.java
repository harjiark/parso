package com.epam.parso.impl;

import java.util.Arrays;

final class BinDecompressor
  implements Decompressor
{
  static final BinDecompressor INSTANCE = new BinDecompressor();

  public byte[] decompressRow(int pageoffset, int srcLength, int resultLength, byte[] page)
  {
    byte[] srcRow = Arrays.copyOfRange(page, pageoffset, srcLength + pageoffset);
    byte[] outRow = new byte[resultLength];
    int srcOffset = 0;
    int outOffset = 0;
    int ctrlBits = 0; int ctrlMask = 0;
    while (srcOffset < srcLength)
    {
      ctrlMask >>= 1;
      if (ctrlMask == 0) {
        ctrlBits = (srcRow[srcOffset] & 0xFF) << 8 | srcRow[(srcOffset + 1)] & 0xFF;
        srcOffset += 2;
        ctrlMask = 32768;
      }

      if ((ctrlBits & ctrlMask) == 0) {
        outRow[(outOffset++)] = srcRow[(srcOffset++)];
        continue;
      }

      int cmd = srcRow[srcOffset] >> 4 & 0xF;
      int cnt = srcRow[(srcOffset++)] & 0xF;

      switch (cmd) {
      case 0:
        cnt += 3;
        for (int i = 0; i < cnt; i++) {
          outRow[(outOffset + i)] = srcRow[srcOffset];
        }
        srcOffset++;
        outOffset += cnt;
        break;
      case 1:
        cnt += ((srcRow[(srcOffset++)] & 0xFF) << 4);
        cnt += 19;
        for (int i = 0; i < cnt; i++) {
          outRow[(outOffset + i)] = srcRow[srcOffset];
        }
        srcOffset++;
        outOffset += cnt;
        break;
      case 2:
        int ofs = cnt + 3;
        ofs += ((srcRow[(srcOffset++)] & 0xFF) << 4);
        cnt = srcRow[(srcOffset++)] & 0xFF;
        cnt += 16;
        System.arraycopy(outRow, outOffset - ofs, outRow, outOffset, cnt);
        outOffset += cnt;
        break;
      default:
        ofs = cnt + 3;
        ofs += ((srcRow[(srcOffset++)] & 0xFF) << 4);
        System.arraycopy(outRow, outOffset - ofs, outRow, outOffset, cmd);
        outOffset += cmd;
      }
    }

    return outRow;
  }
}